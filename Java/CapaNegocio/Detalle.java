/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package CapaNegociio;

/**
 *
 * @author STRIX
 */
public class Detalle extends BoletaVenta{
    //Declarar atributos
    public String cantidad;
    public String descripcion;
    public String subTotal;

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }
    
    //Declarar métodos u operaciones
    public String MostrarInformacion(){
        return "El método MostrarInformacioón aún no ha sido implementado";
    }
    
}
