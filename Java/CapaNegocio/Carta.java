/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package CapaNegociio;

/**
 *
 * @author STRIX
 */
public class Carta {
    //Declarar Atributos
    public String nombrePolleria;
    public String montoPlato;
    public String tiposComida;
    public String tiposBebida;

    public String getNombrePolleria() {
        return nombrePolleria;
    }

    public void setNombrePolleria(String nombrePolleria) {
        this.nombrePolleria = nombrePolleria;
    }

    public String getMontoPlato() {
        return montoPlato;
    }

    public void setMontoPlato(String montoPlato) {
        this.montoPlato = montoPlato;
    }

    public String getTiposComida() {
        return tiposComida;
    }

    public void setTiposComida(String tiposComida) {
        this.tiposComida = tiposComida;
    }

    public String getTiposBebida() {
        return tiposBebida;
    }

    public void setTiposBebida(String tiposBebida) {
        this.tiposBebida = tiposBebida;
    }
   
    //Declarar métodos u operaciones
    public String PedirComida(){
        return "El método PedirComida aún no ha sido implementado";
    }
    public String PedirBebida(){
        return "El método PedirBebida aún no ha sido implementado";
    }
}
