
package CapaNegociio;

import java.util.Date;


public class Persona {
    //Atributos de la clase
   
    public String nombres;
    public String apellidos;
    public String dni;
    public String fechaNac;
    public String nroCelular;

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public String getNroCelular() {
        return nroCelular;
    }
    
    public void setNroCelular(String nroCelular) {
        this.nroCelular = nroCelular;
    }
}
